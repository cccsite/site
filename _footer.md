本網站衍生自[維基百科](https://www.wikipedia.org/)，主要採用 [CC:BY-SA + MIT 授權](/root/程式人媒體/授權.md)

[陳鍾誠](/root/陳鍾誠/README.md) 在 [程式人媒體](/root/程式人媒體/公益集團.md) 邀請您 [捐款給羅慧夫顱顏基金會](https://www.nncf.org/support) ！
