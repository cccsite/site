# 陳鍾誠

主題  | 內容
------|----------------
[履歷](https://www.cakeresume.com/f5611f) | [金門大學](http://www.nqu.edu.tw/) / [資訊工程系](http://www.nqu.edu.tw/educsie/index.php) / [助理教授](http://www.nqu.edu.tw/educsie/index.php?act=blog&code=list&ids=4) 
[課程](./課程/) | [網頁設計](./課程/網頁設計/README.md) / [網站設計進階](./課程/網站設計/README.md) / [計算機結構](./課程/計算機結構/README.md) / [系統程式](./課程/系統程式/README.md) / [軟體工程與演算法](./課程/軟體工程與演算法/README.md) / [人工智慧](./課程/人工智慧/README.md)
[書籍](./書籍/) | [nand2tetris](./書籍/nand2tetris/) / [機率統計](./書籍/機率統計/) / [科學計算](./書籍/科學計算/) / [微積分](./書籍/微積分/) / [C# 程式設計](./書籍/C＃程式設計/)
[技能](./技能/) | [git](./技能/git.md) / [markdown](./技能/markdown.md) / [Shell](./技能/shell.md) / [Linux](./技能/linux.md)
[程式](./程式) | [Python](./程式/Python) / [JavaScript](./程式/JavaScript) / [C](./程式/C) / [Rust](./程式/Rust) / [Dart](./程式/Dart) / [Verilog](./書籍/Verilog)
[作品](./作品) | [散文](./作品/散文/) / [小說](./作品/小說/) / [十分鐘系列](./作品/十分鐘系列/)
[帳號](./帳號) | [Github](https://github.com/ccckmit) / [Gitlab](https://gitlab.com/ccckmit/) / [YouTube](https://www.youtube.com/user/ccckmit) / [SlideShare](http://www.slideshare.net/ccckmit/)  / [Medium](https://medium.com/@ccckmit) / [Facebook](https://www.facebook.com/ccckmit) / [Gmail](mailto://ccckmit@gmail.com)
[蒐藏](./蒐藏) | [影片](./蒐藏/影片.md) / [書籍](./蒐藏/書籍.md) / [程式](./蒐藏/程式.md) / [文章](./蒐藏/文章.md) / [課程](./蒐藏/課程.md) / [工具](./蒐藏/工具.md) / [網站](./蒐藏/網站.md) / [圖靈獎年表](./蒐藏/圖靈獎.md)

